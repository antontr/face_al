import os
import cv2
import numpy as np
import config as cfg

from bounding_box import BoundingBox
from shape_regressor import ShapeRegressor
from time import time


def main():
    images = []
    for i in range(cfg.TEST_IMG_NUM):
        images.append(
            cv2.imread(os.path.join(cfg.TEST_IMAGES_PATH, str(i + 1) + ".jpg"))
        )

    bounding_box = []
    with open(os.path.join(cfg.DATASET_PATH, "boundingbox_test.txt"), "r") as f:
        for line in f:
            tmp = BoundingBox()
            tmp.start_x, tmp.start_y, tmp.width, tmp.height = [
                int(x) for x in line.split()
            ]
            tmp.centroid_x = tmp.start_x + tmp.width / 2
            tmp.centroid_y = tmp.start_y + tmp.height / 2
            bounding_box.append(tmp)

    rng = np.random.default_rng(cfg.SEED)
    regressor = ShapeRegressor(rng)
    regressor.load(os.path.join(cfg.SAVED_MODEL_PATH, "model.txt"))

    while True:
        try:
            stdin = input("Input index: ")
            if stdin == "exit":
                break

            index = int(stdin)

            start_time = time()
            res_shape = regressor.predict(
                images[index][:, :, 0].T.astype("float"), bounding_box[index]
            )
            print(f"Processed in {time()-start_time:.2f} secs")

        except (ValueError, IndexError):
            print("Wrong index, try again\n")
            continue

        res_shape = np.round(res_shape).astype(int)
        image = images[index].copy()
        for shape in res_shape:
            image = cv2.circle(image, shape, 3, (0, 255, 0), -1)
        out_path = os.path.join(cfg.OUTPUT_PATH, str(index + 1) + ".jpg")
        cv2.imwrite(out_path, image)
        print(f"Stored to {out_path}\n")


if __name__ == "__main__":
    main()
