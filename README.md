Python re-implementation of "Face Alignment by Explicit Shape Regression" from [C++ sources](https://github.com/sai-bi/FaceAlignment).


## Usage

Specify paths and other variables in `config.py`

To train model (takes ~hour on full COFW dataset):
```
python3 train_demo.py
```

To predict:
```
python3 test_demo.py
```