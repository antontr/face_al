#import cv2
import numpy as np
import config as cfg


class Fern:
    def __init__(self, rng):
        self.rng = rng
        self.bin_output = None
        self.threshold = None
        self.selected_nearest_landmark_index = None
        self.selected_pixel_locations = None

    def predict(self, image, shape, box, rotation, scale, bx, im):
        xys = (
            scale / 2
            * np.dot(rotation[:, None], self.selected_pixel_locations)
            .reshape(2, -1)
            .T.reshape(-1, 2, 2) * bx
            + shape[self.selected_nearest_landmark_index]
        )
        # xys = np.round(xys).astype(int)
        xys = xys.astype(int)
        xys = np.minimum(xys, im)
        xys = np.maximum(xys, [0, 0])
        res = (
            image[xys[:, 0, 0], xys[:, 0, 1]] - image[xys[:, 1, 0], xys[:, 1, 1]]
            >= self.threshold
        )

        index = 0
        for i in range(cfg.FERN_PIXEL_NUM):
            if res[i]:
                index += 2**i
        return self.bin_output[index]

    def train(
        self,
        candidate_pixel_intensity,
        covariance,
        candidate_pixel_locations,
        nearest_landmark_index,
        regression_targets,
    ):
        self.threshold = []
        selected_pixel_index = []
        self.selected_nearest_landmark_index = []
        self.selected_pixel_locations = np.zeros((cfg.FERN_PIXEL_NUM, 4))

        tmp = np.broadcast_to(
            np.diag(covariance), (cfg.CANDIDATE_PIXEL_NUM, cfg.CANDIDATE_PIXEL_NUM)
        )
        eps = np.abs(tmp + tmp.T - 2 * covariance)

        mask = np.zeros_like(eps)
        mask[eps < 1e-10] = 1
        np.fill_diagonal(eps, 1)
        eps = np.sqrt(eps)

        candidate_pixel_intensityT = candidate_pixel_intensity.T
        candidate_pixel_intensityT_norm = candidate_pixel_intensityT - np.mean(
            candidate_pixel_intensityT, axis=0
        )
        regression_targets = np.array(regression_targets)
        for i in range(cfg.FERN_PIXEL_NUM):
            random_direction = self.rng.uniform(-1.1, 1.1, size=(cfg.LANDMARK_NUM, 2))
            random_direction /= np.linalg.norm(random_direction)

            # projection_result = [np.sum(random_direction * target) for target in regression_targets]
            projection_result = np.sum(
                random_direction[None, :, :] * regression_targets, axis=(1, 2)
            )

            # covariance_projection_density = np.cov(projection_result, candidate_pixel_intensity, bias=True)[0, 1:]
            covariance_projection_density = (
                np.dot(
                    projection_result - np.mean(projection_result),
                    candidate_pixel_intensityT_norm,
                )
                / projection_result.shape[0]
            )

            tmp = np.abs(
                np.subtract.outer(
                    covariance_projection_density, covariance_projection_density
                )
                / eps
            )
            tmp[mask == 1] = -1
            # max_pixel_index_1, max_pixel_index_2 = np.unravel_index(np.argmax(tmp), tmp.shape)
            inds = np.unravel_index(np.argmax(tmp), tmp.shape)
            mask[inds] = 1
            mask[inds[::-1]] = 1

            if inds == (0, 0) or tmp[inds] == -1:
                print("this", i, inds)
                raise ValueError

            selected_pixel_index.append(inds)
            self.selected_nearest_landmark_index.append(
                [nearest_landmark_index[ind] for ind in inds]
            )
            self.selected_pixel_locations[i] = np.concatenate(
                [candidate_pixel_locations[ind] for ind in inds]
            )

            max_diff = np.max(
                np.abs(
                    candidate_pixel_intensity[inds[0]]
                    - candidate_pixel_intensity[inds[1]]
                )
            )
            self.threshold.append(self.rng.uniform(-0.2 * max_diff, 0.2 * max_diff))

        shapes_in_bin = []
        for i in range(2**cfg.FERN_PIXEL_NUM):
            shapes_in_bin.append([])
        for i, target in enumerate(regression_targets):
            index = 0
            for j in range(cfg.FERN_PIXEL_NUM):
                dens1 = candidate_pixel_intensity[selected_pixel_index[j][0], i]
                dens2 = candidate_pixel_intensity[selected_pixel_index[j][1], i]

                if dens1 - dens2 >= self.threshold[j]:
                    index += 2**j

            shapes_in_bin[index].append(i)

        self.bin_output = []
        prediction = np.zeros_like(regression_targets)
        for i, shape in enumerate(shapes_in_bin):
            tmp = np.zeros((cfg.LANDMARK_NUM, 2))
            if shape:
                for index in shape:
                    tmp += regression_targets[index]
                tmp *= 1 / (1000 + len(shape))
                for index in shape:
                    prediction[index] = tmp
            self.bin_output.append(tmp)

        return prediction

    def write(self, f):
        print(cfg.FERN_PIXEL_NUM, file=f)
        print(cfg.LANDMARK_NUM, file=f)

        for i in range(cfg.FERN_PIXEL_NUM):
            print(*self.selected_pixel_locations[i], file=f)
            print(*self.selected_nearest_landmark_index[i], file=f)
            print(self.threshold[i], file=f)

        for bin in self.bin_output:
            print(*bin.reshape(-1), file=f)

    def read(self, f):
        cfg.FERN_PIXEL_NUM = int(f.readline())
        cfg.LANDMARK_NUM = int(f.readline())

        self.threshold = []
        self.selected_nearest_landmark_index = []
        self.selected_pixel_locations = np.zeros((cfg.FERN_PIXEL_NUM, 4))
        for i in range(cfg.FERN_PIXEL_NUM):
            self.selected_pixel_locations[i] = [
                float(num) for num in f.readline().split()
            ]
            self.selected_nearest_landmark_index.append(
                [int(num) for num in f.readline().split()]
            )
            self.threshold.append(float(f.readline()))

        self.selected_pixel_locations = self.selected_pixel_locations.reshape(-1, 2, 2)
        self.selected_pixel_locations = np.transpose(
            self.selected_pixel_locations, axes=[0, 2, 1]
        )

        self.bin_output = []
        for i in range(2**cfg.FERN_PIXEL_NUM):
            self.bin_output.append(
                np.array([float(num) for num in f.readline().split()]).reshape(
                    (cfg.LANDMARK_NUM, 2)
                )
            )

