from time import time
import numpy as np
import config as cfg

from fern import Fern
from utils import project_shape, similarity_transform


class FernCascade:
    def __init__(self, rng):
        self.rng = rng
        self.ferns = None

    def predict(self, image, box, mean_shape, shape, bx, im):
        result = np.zeros((cfg.LANDMARK_NUM, 2))
        rotation, scale = similarity_transform(project_shape(shape, box), mean_shape)
        for fern in self.ferns:
            result += fern.predict(image, shape, box, rotation, scale, bx, im)
        # rotation, scale = similarity_transform(project_shape(shape, box), mean_shape)
        return scale * np.dot(result, rotation.T)

    def train(
        self,
        images,
        current_shapes,
        ground_truth_shapes,
        bounding_box,
        mean_shape,
        curr_level_num,
    ):
        regression_targets = []

        for ground_shape, current_shape, box in zip(
            ground_truth_shapes, current_shapes, bounding_box
        ):
            tmp = project_shape(ground_shape, box) - project_shape(current_shape, box)
            rotation, scale = similarity_transform(
                mean_shape, project_shape(current_shape, box)
            )
            regression_targets.append(scale * np.dot(tmp, rotation.T))

        candidate_pixel_locations = []
        nearest_landmark_index = []
        for i in range(cfg.CANDIDATE_PIXEL_NUM):
            while True:
                x, y = self.rng.uniform(-1, 1), self.rng.uniform(-1, 1)
                if x**2 + y**2 <= 1:
                    break

            min_ind = np.argmin(np.sum((mean_shape - [x, y]) ** 2, axis=1))
            candidate_pixel_locations.append([x, y] - mean_shape[min_ind])
            nearest_landmark_index.append(min_ind)

        densities = []
        for image, box, shape in zip(images, bounding_box, current_shapes):
            tmp = project_shape(shape, box)
            rotation, scale = similarity_transform(tmp, mean_shape)

            project = (
                scale / 2
                * np.sum(rotation[:, None] * candidate_pixel_locations, axis=2)
            )
            project = project.T * [box.width, box.height]

            xy = shape[nearest_landmark_index] + project
            xy = np.minimum(xy, [image.shape[1] - 1, image.shape[0] - 1])
            xy = np.maximum(xy, [0, 0])
            xy = np.round(xy).astype(int)
            densities.append([image[y, x] for x, y in xy])
        # densities = list(zip(*densities))
        densities = np.array(densities, dtype="float").T
        covariance = np.cov(densities, bias=True)

        self.ferns = []
        prediction = np.zeros_like(regression_targets)
        start_time = time()
        print(f"Fern cascades: {curr_level_num} out of {cfg.FIRST_LEVEL_NUM}")
        for i in range(cfg.SECOND_LEVEL_NUM):
            cur_fern = Fern(self.rng)
            cur_pred = cur_fern.train(
                densities,
                covariance,
                candidate_pixel_locations,
                nearest_landmark_index,
                regression_targets,
            )
            prediction += cur_pred
            regression_targets -= cur_pred
            self.ferns.append(cur_fern)

            if (i + 1) % 50 == 0:
                print(f"Ferns: {i+1} out of {cfg.SECOND_LEVEL_NUM}")

                remaining_level_num = (
                    (cfg.FIRST_LEVEL_NUM - curr_level_num) * cfg.SECOND_LEVEL_NUM
                    + cfg.SECOND_LEVEL_NUM - i
                )
                time_remaining = int(
                    np.round((time() - start_time) * remaining_level_num / 50)
                )
                print(np.round((time() - start_time)))
                print(
                    f"Expected remaining time {time_remaining//60} min {time_remaining%60} sec"
                )
                start_time = time()

        for i, (pred, shape, box) in enumerate(
            zip(prediction, current_shapes, bounding_box)
        ):
            rotation, scale = similarity_transform(
                project_shape(shape, box), mean_shape
            )
            prediction[i] = scale * np.dot(pred, rotation.T)

        return prediction

    def write(self, f):
        print(cfg.SECOND_LEVEL_NUM, file=f)
        for fern in self.ferns:
            fern.write(f)

    def read(self, f):
        cfg.SECOND_LEVEL_NUM = int(f.readline())
        self.ferns = []
        for i in range(cfg.SECOND_LEVEL_NUM):
            fern = Fern(self.rng)
            fern.read(f)
            self.ferns.append(fern)
