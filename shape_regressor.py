# import cv2
import numpy as np
import config as cfg
from itertools import chain

from bounding_box import BoundingBox
from fern_cascade import FernCascade
from utils import project_shape, reproject_shape, get_mean_shape


class ShapeRegressor:
    def __init__(self, rng):
        self.rng = rng
        self.fern_cascades = None
        self.mean_shape = None
        self.training_shapes = None
        self.bounding_box = None

    def predict(self, image, box):
        result = np.zeros((cfg.LANDMARK_NUM, 2))
        im = np.array([image.shape[0] - 1, image.shape[1] - 1])
        bx = np.array([box.width, box.height])
        for i in range(cfg.INITIAL_NUMBER):
            index = self.rng.integers(len(self.training_shapes))
            current_shape = self.training_shapes[index]
            current_box = self.bounding_box[index]
            current_shape = project_shape(current_shape, current_box)
            current_shape = reproject_shape(current_shape, box)

            for cascade in self.fern_cascades:
                prediction = cascade.predict(
                    image, box, self.mean_shape, current_shape, bx, im
                )
                current_shape = prediction + project_shape(current_shape, box)
                current_shape = reproject_shape(current_shape, box)
            result += current_shape

        return result / cfg.INITIAL_NUMBER

    def train(self, images, ground_truth_shapes, bounding_box):
        self.training_shapes = ground_truth_shapes
        self.bounding_box = bounding_box

        augmented_images = []
        augmented_bounding_box = []
        augmented_ground_truth_shapes = []
        current_shapes = []

        for i, image in enumerate(images):
            for j in range(cfg.INITIAL_NUMBER):
                augmented_images.append(images[i])
                augmented_ground_truth_shapes.append(ground_truth_shapes[i])
                augmented_bounding_box.append(bounding_box[i])

                index = self.rng.choice(
                    list(chain(range(0, i), range(i + 1, len(images))))
                )
                tmp = ground_truth_shapes[index]
                tmp = project_shape(tmp, bounding_box[index])
                tmp = reproject_shape(tmp, bounding_box[i])
                current_shapes.append(tmp)

        self.mean_shape = get_mean_shape(ground_truth_shapes, bounding_box)

        self.fern_cascades = []
        for i in range(cfg.FIRST_LEVEL_NUM):
            print(f"Training fern cascades: {i+1} out of {cfg.FIRST_LEVEL_NUM}")

            csc = FernCascade(self.rng)
            prediction = csc.train(
                augmented_images,
                current_shapes,
                augmented_ground_truth_shapes,
                augmented_bounding_box,
                self.mean_shape,
                i + 1,
            )
            self.fern_cascades.append(csc)

            for j, (pred, shape, box) in enumerate(
                zip(prediction, current_shapes, augmented_bounding_box)
            ):
                shape = pred + project_shape(shape, box)
                current_shapes[j] = reproject_shape(shape, box)

    def write(self, f):
        print(cfg.FIRST_LEVEL_NUM, file=f)
        print(cfg.LANDMARK_NUM, file=f)
        print(*self.mean_shape.reshape(-1), file=f)

        print(len(self.training_shapes), file=f)
        for box in self.bounding_box:
            print(
                *[
                    box.start_x,
                    box.start_y,
                    box.width,
                    box.height,
                    box.centroid_x,
                    box.centroid_y,
                ],
                file=f,
            )
        for shape in self.training_shapes:
            print(*shape.reshape(-1), file=f)

        for csc in self.fern_cascades:
            csc.write(f)

    def read(self, f):
        cfg.FIRST_LEVEL_NUM = int(f.readline())
        cfg.LANDMARK_NUM = int(f.readline())
        self.mean_shape = np.array(
            [float(num) for num in f.readline().split()]
        ).reshape((cfg.LANDMARK_NUM, 2))

        training_num = int(f.readline())
        self.bounding_box = []
        for i in range(training_num):
            box = BoundingBox()
            (
                box.start_x,
                box.start_y,
                box.width,
                box.height,
                box.centroid_x,
                box.centroid_y,
            ) = [float(num) for num in f.readline().split()]
            self.bounding_box.append(box)

        self.training_shapes = []
        for i in range(training_num):
            self.training_shapes.append(
                np.array([float(num) for num in f.readline().split()]).reshape(
                    (cfg.LANDMARK_NUM, 2)
                )
            )

        self.fern_cascades = []
        for i in range(cfg.FIRST_LEVEL_NUM):
            csc = FernCascade(self.rng)
            csc.read(f)
            self.fern_cascades.append(csc)

    def load(self, file_name):
        print("Loading model...")
        with open(file_name, "r") as f:
            self.read(f)
        print("Load completed")

    def save(self, file_name):
        print("Saving model...")
        with open(file_name, "w") as f:
            self.write(f)
        print("Save completed")
