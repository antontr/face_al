import os
import cv2
import numpy as np
import config as cfg

from bounding_box import BoundingBox
from shape_regressor import ShapeRegressor


def main():
    images = []
    for i in range(cfg.TRAIN_IMG_NUM):
        images.append(
            cv2.imread(os.path.join(cfg.TRAIN_IMAGES_PATH, str(i + 1) + ".jpg"), 0)
        )
        pass

    bounding_box = []
    with open(os.path.join(cfg.DATASET_PATH, "boundingbox.txt"), "r") as f:
        for i, line in enumerate(f):
            if i >= len(images):
                break
            tmp = BoundingBox()
            tmp.start_x, tmp.start_y, tmp.width, tmp.height = [
                int(x) for x in line.split()
            ]
            tmp.centroid_x = tmp.start_x + tmp.width / 2
            tmp.centroid_y = tmp.start_y + tmp.height / 2
            bounding_box.append(tmp)

    ground_truth_shapes = []
    with open(os.path.join(cfg.DATASET_PATH, "keypoints.txt"), "r") as f:
        for i, line in enumerate(f):
            if i >= len(images):
                break
            tmp = np.array([float(x) for x in line.split()])
            ground_truth_shapes.append(tmp.reshape(2, cfg.LANDMARK_NUM).T)

    rng = np.random.default_rng(cfg.SEED)
    regressor = ShapeRegressor(rng)
    regressor.train(images, ground_truth_shapes, bounding_box)
    print("Train completed")
    regressor.save(os.path.join(cfg.SAVED_MODEL_PATH, "model.txt"))


if __name__ == "__main__":
    main()
