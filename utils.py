import cv2
import numpy as np
import config as cfg


def get_mean_shape(shapes, bounding_boxes):
    result = np.zeros((cfg.LANDMARK_NUM, 2))
    for shape, box in zip(shapes, bounding_boxes):
        result += project_shape(shape, box)
    return result / len(shapes)


def project_shape(shape, bounding_box):
    return (
        2
        * (shape - [bounding_box.centroid_x, bounding_box.centroid_y])
        / [bounding_box.width, bounding_box.height]
    )


def reproject_shape(shape, bounding_box):
    return [bounding_box.centroid_x, bounding_box.centroid_y] + [
        bounding_box.width,
        bounding_box.height,
    ] * shape / 2


def similarity_transform(input1, input2):
    shape1 = input1 - np.mean(input1, axis=0)
    shape2 = input2 - np.mean(input2, axis=0)

    # s1 = np.linalg.norm(np.cov(shape1, rowvar=False))
    # s2 = np.linalg.norm(np.cov(shape2, rowvar=False))
    # s1 = np.sqrt(s1)
    # s2 = np.sqrt(s2)
    s1 = np.sqrt(2 * np.sum((shape1[:, 0] - np.mean(shape1, axis=1)) ** 2))
    s2 = np.sqrt(2 * np.sum((shape2[:, 0] - np.mean(shape2, axis=1)) ** 2))
    scale = s1 / s2

    shape1 /= s1
    shape2 /= s2

    num = np.sum(shape1[:, 1] * shape2[:, 0] - shape1[:, 0] * shape2[:, 1])
    den = np.sum(shape1[:, 0] * shape2[:, 0] + shape1[:, 1] * shape2[:, 1])
    norm = np.sqrt(num**2 + den**2)

    rotation = np.array([[den, -num], [num, den]]) / norm
    return rotation, scale


def calculate_covariance(v1, v2):
    return np.mean((v1 - np.mean(v1, axis=0)) * (v2 - np.mean(v2, axis=0)))
